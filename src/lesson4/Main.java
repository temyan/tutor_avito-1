package lesson4;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    //
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Двумерные массивы
        int n = 2;
        int m = 3;
        int[][] ar = new int[n][]; // Матрица
        // n -> строки {1, 2, 3, 4, 5}
        // m -> столбцы {1, 2, 3, 4, 5}
        //              {3, 2, 1, 5, 10}
        int[][] ar2 = new int[n][m];
        int[] ar3[] = new int[n][m];
        int ar4[][] = new int[n][m];
        System.out.println(ar2.length); // n
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                ar2[i][j] = new Random().nextInt();
            }
        }
        System.out.println(Arrays.deepToString(ar2));

        // Строка в Java
        // Массив из символов (усовершенствованный)
        // В строках всегда ", в символах всегда '
        // 1) Best variant
        String str1 = "Hello World";
        // Строка попадает в "String pool". String pool -> мини-стэк (кэш).
        String str2 = "Hello World"; // В String pool str2 == str
        System.out.println(str1 == str2); // Сравниваются значения (потому Pool) true

        // 2)
        String s1 = new String("Hello World"); // выделили память
        String s2 = new String("Hello World"); // выделили память
        System.out.println(s1 == s2); // сравниваются ссылки false
        System.out.println(s1.equals("Hello World")); // сравниваются значения true

        // Методы у строк
        // str1.length(); -> этот метод возвращает длину строки
        // Пробелы в конце, между, в начале - тоже символы и тоже входят в длину строки
        // Иначе говоря, длина строки - это всё от первого символа после первой ", до последнего символа последней "
        System.out.println(str1.length());
        // str1.equals(str2); -> true/false в зависимости от одинаковости строк (по значению)
        // equals строго рекомендуется использовать всегда при сравнении по значению (==)
        System.out.println(s1.equals(s2));
        System.out.println(s1.charAt(2)); // аналог ar[i] или ar[2]

        System.out.println("HELLOIUEHOIWEH".toLowerCase()); // переводит всю строку в нижний регистр
        System.out.println("irohoib".toUpperCase()); // переводит всю строку в верхний регистр
        System.out.println(s1.replace('l', ' ')); // меняет у строки все символы в первом параметре на какой-то другой символ во втором параметре

        // Почти (!) все методы возвращают новую строку
        s1 = s1.toLowerCase();
        // code
        s1 = s1.replace('l', 'k');
        s1 = s1.toUpperCase();
        System.out.println(s1);

        String x = s1.toLowerCase().replace('l', 'k').toUpperCase();
        System.out.println(x);

//        String readLine = in.nextLine(); // nextLine считывает целую строку
//        System.out.println(readLine);
//        String read = in.next(); // next() считывает слово (один String)
//        System.out.println(read);

        // Посчитать количество цифр в слове
        String v = in.next(); // "Q" -> строка
        int cnt = 0;
        for (int i = 0; i < v.length(); i++) {
            if (v.charAt(i) == '0' || v.charAt(i) == '1'
                    || v.charAt(i) == '2' || v.charAt(i) == '3' || v.charAt(i) == '4'
                    || v.charAt(i) == '5' || v.charAt(i) == '6' || v.charAt(i) == '7'
                    || v.charAt(i) == '8' || v.charAt(i) == '9') {
                cnt++;
            }
        }
        System.out.println(cnt);

        /* todo
            Вводится строка (гарантированно одна буква (англ))
            Необходимо вывести в консоль предыдущую и последующую букву по раскладке QWERTY (раскладка клавиатуры)
            Example:
            Input: y
            Output: t u
            Example:
            Input: m
            Output: n
         */
    }
}
