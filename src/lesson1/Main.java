package lesson1;

import java.util.Scanner;

public class Main {
    // Вот, создай нам новый класс
    // ООП ЯП
    // ; обязательно в конце каждой строчки кода
    public static void main(String[] args) {
        // метод позволяет запускать весь наш код (Java-код)
        // System - класс (библиотека) для работы с системой
        // out - поле (говорит джаве выводить)
        // println (выводит на новой строке)
        // print (оставляет на такой строке)
        System.out.println("Hello World");

        // 1)
        // *тип данных* название переменной = a) если у нас ссылочный тип данных - через ключевое слово new *название класса*
        //                                    б) если у нас примитивный тип данных - сразу значение;
        int a = 100; // Integer -> целые числа
        // 2)
        int b;
        b = 2;

        // Примитивные и Ссылочные
        // Стэк (Stack) и Куче (Heap)
        // Stack - RAM доступ быстрый, но места мало (доступа к стэку у нас нет)
        // Heap - HDD доступ медленнее, но места больше

        // примитивы начинаются и пишутся всеми маленькими буквами
        // у примитивов нет методов/полей/через точку ничего нельзя вызвать

        byte var1; // целое число, -128...127 -2^7...2^7-1 0 считается за положительное число (1 byte)
        short var2; // целое число, -2^15...2^15-1 (2 byte)
        int var3; // целое число -2^31...2^31-1 (4 byte)
        long var4; // целое число -2^63...2^63-1 (8 byte)

        float var5; // дробное число -2^31...2^31-1 (4 byte) 6-7 знаков
        double var6; // дробное число -2^63...2^63-1 (8 byte) 12-14 знаков

        boolean var7; // true/false (1 byte)
        char var8; // символ 0...65535.
        // Unicode (таблица кодировки)
        // a <-> 97
        // b <-> 98

        // Ссылочные
        // Начинаются с большой буквы (Scanner, String, lesson1.Main, Integer)
        // при инициализации присутствует ключевое слово new (int[] array = new int[])
        // Примитивы можно сравнивать через ==, а ссылочные типы данных через == сравнивают ссылку, а не значение

        int x = Integer.MAX_VALUE; // + 1 -> -2147483648 + 2147483646 -> -2
        // [max] [max] [max] [max] + 1 -> [min] [min] [min] [min]
        // int + long -> long
        long z = x + x + 3 + 11523125123L; // long > int
        // int p = z + z;
        // 2147483647 + 1 -> -2147483648
        // -2147483648 - 1 -> 2147483647 - 99 -> ...548
        System.out.println(x - 100); // -> int (default) long (L)

        // cast
        int m = (int) z; // 9999999999 -> 2147483647 + 1 -> -2147483648 ... -> 999999999
        System.out.println(m);
        int i = 123;
        double o = (double) i;
        double wq = 42.951;
        int u = (int) wq;
        System.out.println(u);
        System.out.println((double) i);


        // ASCII -> только цифры + английский алфавит
        // UTF-8 (русский алфавит)
        // UTF-16 (китайский классический)
        // Unicode -> все на свете
        int g = 97;
        char ch = (char) g;
        char yo = 'o'; // символы указываются в одинарных кавычках (причем только символ)
        int numOfYo = (int) yo;
        System.out.println(numOfYo);
        System.out.println(ch);

        Scanner in = new Scanner(System.in); // ввод с клавиатуры
        int v1 = in.nextInt();
        int v2 = in.nextInt();
        System.out.println(v1 + v2);
        v1 = v1 * 2; // v1=20
        System.out.println(v1 + v2);




        int k = 2147483647;
        System.out.println(k + 1); // +1 -> -2147483648
        // char: 0...65535 (2^15-1)

    }

    /* todo
        Вводится число N с клавиатуры (0 <= N <= 10000) |
        Вывести символ по этому числу
        Example:
        Input: 98
        Output: b
     */
}
